from django.contrib import admin
from django.urls import path
from todos.views import (
    show_todolist,
    todolist_detail,
    create_todolist,
    edit_todolist,
    delete_todolist,
    create_task,
    edit_task,
)

urlpatterns = [
    path("", show_todolist, name="todo_list_list"),
    path("<int:id>/", todolist_detail, name="todolist_detail"),
    path("/create", create_todolist, name="todo_list_create"),
    path("<int:id>/update/", edit_todolist, name="todo_list_update"),
    path("<int:id>/delete/", delete_todolist, name="todo_list_delete"),
    path("items/create/", create_task, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_task, name="todo_item_update"),
]
