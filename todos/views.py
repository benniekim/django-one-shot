from django.shortcuts import render, get_object_or_404, redirect, render
from todos.models import TodoList, TodoItem
from todos.forms import NewlistForm, TaskForm

# Create your views here.
def show_todolist(request):
    todolist = TodoList.objects.all()
    context = {
        "todo_list": todolist,
    }
    return render(request, "todos/mylist.html", context)


def todolist_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    print("detail:::::", detail)
    context = {
        "show_detail": detail,
    }
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = NewlistForm(request.POST)
        if form.is_valid():
            # list = form.save(False)
            # list.author = request.user
            list = form.save()
            # list.save()
            return redirect("todolist_detail", id=list.id)
    else:
        form = NewlistForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_todolist(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = NewlistForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todolist_detail", id=list.id)
    else:
        form = NewlistForm(instance=list)
    context = {"form": form}

    return render(request, "todos/edit.html", context)


def delete_todolist(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todolist_detail", id=item.list.id)
    else:
        form = TaskForm()

    context = {
        "form": form,
    }
    return render(request, "todos/createtask.html", context)


def edit_task(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todolist_detail", id=item.list.id)
    else:
        form = TaskForm(instance=item)
    context = {"form": form}

    return render(request, "todos/updatetask.html", context)
