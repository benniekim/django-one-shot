"""brain_two URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from todos.views import (
    show_todolist,
    todolist_detail,
    create_todolist,
    edit_todolist,
    delete_todolist,
    create_task,
    edit_task,
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("todos/", show_todolist, name="todo_list_list"),
    path("<int:id>/", todolist_detail, name="todolist_detail"),
    path("todos/create/", create_todolist, name="todo_list_create"),
    path("todos/<int:id>/edit/", edit_todolist, name="todo_list_update"),
    path("todos/<int:id>/delete/", delete_todolist, name="todo_list_delete"),
    path("todos/items/create/", create_task, name="todo_item_create"),
    path("todos/items/<int:id>/edit/", edit_task, name="todo_item_update"),
]
